﻿using CYLYAdditions.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CYLYAdditions.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BuyingGroupController : ApiController
    {
        private IBuyingGroupService _buyService;

        public BuyingGroupController(IBuyingGroupService buyingGroupService)
        {
            _buyService = buyingGroupService;
        }

        [HttpGet, Route("api/BuyingGroup/All")]
        public HttpResponseMessage GetAllBuyingGroups()
        {
            var buys = _buyService.Get().OrderBy(bg => bg.grp);
            return Request.CreateResponse(HttpStatusCode.OK, buys);
        }
    }
}
