﻿using CYLYAdditions.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CYLYAdditions.Api.Controllers
{
    [EnableCors(origins:"*", headers:"*", methods: "*")]
    public class SalesRepController : ApiController
    {
        private ISalesRepService _repService;
        private ITeamService _teamService;

        public SalesRepController(ISalesRepService repService, ITeamService teamService)
        {
            _repService = repService;
            _teamService = teamService;
        }

        [HttpGet, Route("api/SalesRep/All/{company}")]
        public HttpResponseMessage GetAllReps(string company)
        {
            var reps = _repService.Get(company);
            return Request.CreateResponse(HttpStatusCode.OK, reps);
        }

        [HttpGet, Route("api/SalesRep/Team/{username}")]
        public HttpResponseMessage GetTeam(string username)
        {
            var team = _teamService.Get(username);
            return Request.CreateResponse(HttpStatusCode.OK, team);
        }
    }
}
