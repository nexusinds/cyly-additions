﻿using CYLYAdditions.DAL.Models;
using CYLYAdditions.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CYLYAdditions.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdditionController : ApiController
    {
        private IAdditionService _additionService;

        public AdditionController(IAdditionService additionService)
        {
            _additionService = additionService;
        }

        [HttpGet, Route("api/Addition/All")]
        public HttpResponseMessage GetAllAdditions()
        {
            var additions = _additionService.Get().OrderByDescending(a => a.Yr).ThenByDescending(a => a.Mth).ThenBy(a => a.SalesRepName);
            return Request.CreateResponse(HttpStatusCode.OK, additions);
        }

        [HttpGet, Route("api/Addition/{id}")]
        public HttpResponseMessage Get(int id)
        {
            var addition = _additionService.Get(id);
            return Request.CreateResponse(HttpStatusCode.OK, addition);
        }

        [HttpGet, Route("api/Addition/History")]
        public HttpResponseMessage GetHistory()
        {
            var history = _additionService.GetHistory();
            return Request.CreateResponse(HttpStatusCode.OK, history);
        }

        [HttpPost, Route("api/Addition/Create")]
        public HttpResponseMessage CreateAddition([FromBody] Additions addition)
        {
            var insertedAddition = _additionService.AddAddition(addition);
            if(insertedAddition?.Id == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "An error occured. Therefore the addition was not saved.");
            return Request.CreateResponse(HttpStatusCode.OK, insertedAddition);
        }

        [HttpPost, Route("api/Addition/Amend")]
        public HttpResponseMessage AmendAddition([FromBody] Additions addition)
        {
            _additionService.AmendAddition(addition);
            return Request.CreateResponse(HttpStatusCode.OK, addition);
        }

        [HttpPost, Route("api/Addition/Delete/{id}")]
        public HttpResponseMessage DeleteAddition(int id)
        {
            var addition = _additionService.Get(id);
            _additionService.DeleteAddition(addition);
            return Request.CreateResponse(HttpStatusCode.OK, addition);
        }
    }
}
