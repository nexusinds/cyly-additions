using CYLYAdditions.DAL.Context;
using CYLYAdditions.DAL.Services;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace CYLYAdditions.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<ICylyContext, CylyContext>();
            container.RegisterType<IProjectLogContext, ProjectLogContext>();
            container.RegisterType<IAdditionService, AdditionService>();
            container.RegisterType<ISalesRepService, SalesRepService>();
            container.RegisterType<ITeamService, TeamService>();
            container.RegisterType<IBuyingGroupService, BuyingGroupService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}