﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYLYAdditions.DAL.Models
{
    [Table("nexus_buying_group")]
    public class BuyingGroup
    {
        public int id { get; set; }
        public string grp { get; set; }
    }
}
