﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYLYAdditions.DAL.Models
{
    public class SalesRep
    {
        public string Company { get; set; }
        public string SalesRepCode { get; set; }
        public string Name { get; set; }
        public bool InActive { get; set; }
    }
}
