﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYLYAdditions.DAL.Models
{
    public class TeamMembership
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int TeamId { get; set; }
        public bool Primary { get; set; }
    }
}
