﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYLYAdditions.DAL.Models
{
    [Table("nexus_cyly_additions")]
    public class Additions
    {
        public int Id { get; set; }
        public string SalesRepName { get; set; }
        public int Yr { get; set; }
        public int Mth { get; set; }
        public string Team { get; set; }
        public string OppositeSalesRepName { get; set; }
        public decimal? SaleValue_g { get; set; }
        public decimal? SaleCost_g { get; set; }
        public decimal? KFValue_g { get; set; }
        public decimal? KFCost_g { get; set; }
        public decimal? ClearanceValue_g { get; set; }
        public decimal? ClearanceCost_g { get; set; }
        public string Reason { get; set; }
        public decimal SaleMargContr { get; set; }
        public decimal KFMargContr { get; set; }
        public decimal ClearanceMargContr { get; set; }
        public int? BuyingGroupId { get; set; }
    }
}
