﻿using CYLYAdditions.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYLYAdditions.DAL.Context
{
    public class ProjectLogContext : DbContext, IProjectLogContext
    {
        public ProjectLogContext() : base("name=ProjectLog") { }

        public virtual IDbSet<TeamMembership> TeamMemberships { get; set; }
        public virtual IDbSet<Team> Teams { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Team>()
                        .HasKey(t => t.Id);

            modelBuilder.Entity<TeamMembership>()
                        .HasKey(tm => tm.Id);

            base.OnModelCreating(modelBuilder);
        }
    }

    public interface IProjectLogContext
    {
        IDbSet<TeamMembership> TeamMemberships { get; set; }
        IDbSet<Team> Teams { get; set; }
    }
}
