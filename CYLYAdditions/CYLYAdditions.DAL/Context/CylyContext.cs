﻿using CYLYAdditions.DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYLYAdditions.DAL.Context
{
    public class CylyContext : DbContext, ICylyContext
    {
        public CylyContext() : base("name=Erp10") { }

        public virtual IDbSet<Additions> Additions { get; set; }
        public virtual IDbSet<AdditionHistory> History { get; set; }
        public virtual IDbSet<SalesRep> SalesReps { get; set; }
        public virtual IDbSet<BuyingGroup> BuyingGroups { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Additions>()
                        .HasKey(a => a.Id)
                        .Property(a => a.Id)
                        .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<SalesRep>()
                        .HasKey(sr => new { sr.Company, sr.SalesRepCode });

            modelBuilder.Entity<BuyingGroup>()
                        .HasKey(bg => bg.id);

            base.OnModelCreating(modelBuilder);
        }

        public void Save()
        {
            SaveChanges();
        }

        public async Task SaveAsync()
        {
            await SaveChangesAsync();
            return;
        }

        public void SetModified<T>(T entity) where T : class
        {
            var set = Set<T>();
            set.Attach(entity);
            Entry(entity).State = EntityState.Modified;
        }

        public void Remove<T>(T entity) where T : class
        {
            var set = Set<T>();
            if (Entry(entity).State == EntityState.Detached)
            {
                set.Attach(entity);
            }
            set.Remove(entity);
        }

        public void Detach<T>(T entity) where T : class
        {
            if (entity == null)
                throw new ArgumentNullException("entity", "entity cannot be null.");

            Entry(entity).State = EntityState.Detached;
        }
    }

    public interface ICylyContext
    {
        void Save();
        Task SaveAsync();
        void SetModified<T>(T entity) where T : class;
        void Remove<T>(T entity) where T : class;
        void Detach<T>(T entity) where T : class;

        IDbSet<Additions> Additions { get; set; }
        IDbSet<AdditionHistory> History { get; set; }
        IDbSet<SalesRep> SalesReps { get; set; }
        IDbSet<BuyingGroup> BuyingGroups { get; set; }
    }
}
