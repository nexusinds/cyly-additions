﻿using CYLYAdditions.DAL.Context;
using CYLYAdditions.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYLYAdditions.DAL.Services
{
    public class BuyingGroupService : IBuyingGroupService
    {
        private ICylyContext _ctx;

        public BuyingGroupService(ICylyContext ctx)
        {
            _ctx = ctx;
        }

        public IEnumerable<BuyingGroup> Get()
        {
           return _ctx.BuyingGroups;
        }
    }

    public interface IBuyingGroupService
    {
        IEnumerable<BuyingGroup> Get();
    }
}
