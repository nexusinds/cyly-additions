﻿using CYLYAdditions.DAL.Context;
using CYLYAdditions.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYLYAdditions.DAL.Services
{
    public class SalesRepService : ISalesRepService
    {
        private ICylyContext _ctx;

        public SalesRepService(ICylyContext ctx)
        {
            _ctx = ctx;
        }

        public IEnumerable<SalesRep> Get(string company)
        {
            if (company == "null")
                return _ctx.SalesReps.Where(sr => sr.InActive == false)
                                    .OrderBy(sr => sr.Name);
            else
                return _ctx.SalesReps.Where(sr => sr.Company == company &&
                                                sr.InActive == false)
                                    .OrderBy(sr => sr.Name);
        }
    }

    public interface ISalesRepService
    {
        IEnumerable<SalesRep> Get(string company);
    }
}
