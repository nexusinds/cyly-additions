﻿using CYLYAdditions.DAL.Context;
using CYLYAdditions.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CYLYAdditions.DAL.Services
{
    public class AdditionService : IAdditionService
    {
        private ICylyContext _ctx;

        public AdditionService(ICylyContext ctx)
        {
            _ctx = ctx;
        }

        public IEnumerable<Additions> Get()
        {
            return _ctx.Additions;
        }

        public Additions Get(int id)
        {
            return _ctx.Additions.Where(a => a.Id == id).FirstOrDefault();
        }

        public IEnumerable<AdditionHistory> GetHistory()
        {
            return _ctx.History;
        }

        public Additions AddAddition(Additions addition)
        {
            var inserted = _ctx.Additions.Add(addition);
            var insertedHistory = _ctx.History.Add(CreateHistory(addition));
            _ctx.Save();
            return inserted;
        }

        public void AmendAddition(Additions addition)
        {
            _ctx.SetModified(addition);
            var insertedHistory = _ctx.History.Add(CreateHistory(addition));
            _ctx.Save();
        }

        public void DeleteAddition(Additions addition)
        {
            _ctx.Additions.Remove(addition);
            _ctx.Save();
        }

        private AdditionHistory CreateHistory(Additions addition)
        {
            return new AdditionHistory()
            {
                ClearanceCost_g = addition.ClearanceCost_g,
                ClearanceValue_g = addition.ClearanceValue_g,
                ClearanceMargContr = addition.ClearanceMargContr,
                KFCost_g = addition.KFCost_g,
                KFValue_g = addition.KFValue_g,
                KFMargContr = addition.KFMargContr,
                SaleCost_g = addition.SaleCost_g,
                SaleValue_g = addition.SaleValue_g,
                SaleMargContr = addition.SaleMargContr,
                Mth = addition.Mth,
                OppositeSalesRepName = addition.OppositeSalesRepName,
                Reason = addition.Reason,
                SalesRepName = addition.SalesRepName,
                Team = addition.Team,
                Yr = addition.Yr,
                BuyingGroupId = addition.BuyingGroupId
            };
        }
    }

    public interface IAdditionService
    {
        IEnumerable<Additions> Get();
        Additions Get(int id);
        IEnumerable<AdditionHistory> GetHistory();
        Additions AddAddition(Additions addition);
        void AmendAddition(Additions addition);
        void DeleteAddition(Additions addition);
    }
}
