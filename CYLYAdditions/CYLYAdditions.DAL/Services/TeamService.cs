﻿using CYLYAdditions.DAL.Context;
using CYLYAdditions.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYLYAdditions.DAL.Services
{
    public class TeamService : ITeamService
    {
        private IProjectLogContext _ctx;
        private const string _company = "COMP01";

        public TeamService(IProjectLogContext ctx)
        {
            _ctx = ctx;
        }

        public string Get(string username)
        {
            return _ctx.TeamMemberships.Where(tm => tm.UserId.Equals(username))
                                        .Join(_ctx.Teams,
                                                tm => tm.TeamId,
                                                t => t.Id,
                                                (tm, t) => new { Memberships = tm, Teams = t })
                                        .OrderByDescending(t => t.Memberships.Primary)
                                        .Select(t => t.Teams.Name)
                                        .FirstOrDefault();
        }
    }

    public interface ITeamService
    {
        string Get(string username);
    }
}
