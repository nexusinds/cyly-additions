export class SalesRep {
    public Company: string;
    public SalesRepCode: string;
    public Name: string;
    public InActive: boolean;
}