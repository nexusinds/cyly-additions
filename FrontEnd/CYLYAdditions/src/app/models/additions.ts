export class Additions {
    public Id: number;
    public SalesRepName: string;
    public Yr: number;
    public Mth: number;
    public Team: string;
    public OppositeSalesRepName: string;
    public SaleValue_g: number;
    public SaleCost_g: number;
    public KFValue_g: number;
    public KFCost_g: number;
    public ClearanceValue_g: number;
    public ClearanceCost_g: number;
    public Reason: string;
    public SaleMargContr: number;
    public KFMargContr: number;
    public ClearanceMargContr: number;
    public BuyingGroupId: number;
}
