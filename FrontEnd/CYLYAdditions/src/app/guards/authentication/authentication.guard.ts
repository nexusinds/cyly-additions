import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs';
import { AdalService } from './../../services/adal/adal/adal.service';

@Injectable()
export class AuthenticationGuard implements CanActivate {
    constructor(private router: Router, private adalService: AdalService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        let navigationExtras: NavigationExtras = {
            queryParams: { 'redirectUrl': route.url }
        };

        if (!this.adalService.userInfo) {
            this.router.navigate(['login'], navigationExtras);
        }
        return true;
    }
}
