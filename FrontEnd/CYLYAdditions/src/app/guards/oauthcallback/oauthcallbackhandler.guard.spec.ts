import { TestBed, async, inject } from '@angular/core/testing';

import { OauthcallbackhandlerGuard } from './oauthcallbackhandler.guard';

describe('OauthcallbackhandlerGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OauthcallbackhandlerGuard]
    });
  });

  it('should ...', inject([OauthcallbackhandlerGuard], (guard: OauthcallbackhandlerGuard) => {
    expect(guard).toBeTruthy();
  }));
});
