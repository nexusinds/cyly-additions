import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor() { }

  public get getAdalConfig(): any {
    return {
      tenant: '140009e5-3a89-40d8-bd74-7ed6fc69a003',
      clientId: 'b2e70d79-916c-4f08-b267-3c3d96b3bf0c',
      redirectUri: 'https://localhost:4200/',
      // redirectUri: 'https://internal.staging.luce.co:8043/CYLY/',
      // redirectUri: 'https://internal.app.luce.co:8043/CYLY/',
      postLogoutRedirectUrl: window.location.origin,
      endpoints: {
        graphApiUri: 'https://graph.microsoft.com'
      },
      cacheLocation: 'localStorage'
    };
  }
}
