import { Injectable } from '@angular/core';
import 'expose-loader?AuthenticationContext!../../../../../node_modules/adal-angular/lib/adal.js';
import { ConfigService } from '../config/config.service';

const createAuthCxtFn: adal.AuthenticationContextStatic = AuthenticationContext;

@Injectable({
  providedIn: 'root'
})
export class AdalService {
  private context: adal.AuthenticationContext;
  public token;

  constructor(private configService: ConfigService) {
    this.context = new createAuthCxtFn(configService.getAdalConfig);
  }

  login() {
    this.context.login();
  }

  logout() {
    this.context.logOut();
  }

  handleCallback() {
    this.context.handleWindowCallback();
  }

  public get userInfo() {
    return this.context.getCachedUser();
  }

  getLoginMessage(user) {
    if (this.userInfo != null) {
      return 'Welcome, ' + this.userInfo.profile.name;
    }
  }

  public get getRoles() {
    if (this.userInfo !== null) {
      return this.userInfo.profile.roles;
    }
    return [];
  }

  public get accessToken() {
    return this.context.getCachedToken(this.configService.getAdalConfig.clientId);
  }

  public get accessTokenGraph() {
    return this.context.acquireToken(this.configService.getAdalConfig.endpoints.graphApiUri, function (error, token) {
        if (error) {
          console.log('ADAL error occurred: ' + error);
          return null;
        } else {
          this.token = token;
          return token;
        }
      }.bind(this));
  }

  public get isAuthenticated() {
    return this.userInfo && this.accessToken;
  }
}
