import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { GlobalService } from '../global/global.service';
import { BuyingGroups } from '../../models/buying-groups';

@Injectable({
  providedIn: 'root'
})
export class BuyingGroupsService {
  private endpoint: string;
  private httpOptions: any;

  constructor(private _globalService: GlobalService,
              private _http: HttpClient) {
                this.endpoint = `${this._globalService.GetEndpoint()}BuyingGroup/`;
                this.httpOptions = {
                  headers: this._globalService.headers
                };
              }

  public GetAll = (): Observable<Array<BuyingGroups>> => {
    return this._http.get<Array<BuyingGroups>>(`${this.endpoint}All`, { headers: this.httpOptions });
  }
}
