import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  public headers = new Headers({'Content-Type': 'application/json; charset=utf-8'});

  constructor() { }

  public GetEndpoint = (): string => {
    const endpoint = window.location.href;
    if (endpoint.indexOf('localhost') > -1) {
      return 'https://localhost:44346/api/';
    } else if (endpoint.indexOf('staging.luce.co') > -1) {
      return 'https://internal.staging.luce.co:8043/CYLYApi/api/';
    } else {
      return 'https://internal.app.luce.co:8043/CYLYApi/api/';
    }
  }
}
