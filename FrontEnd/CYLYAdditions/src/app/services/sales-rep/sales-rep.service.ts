import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { GlobalService } from '../global/global.service';
import { SalesRep } from '../../models/sales-rep';

@Injectable({
  providedIn: 'root'
})
export class SalesRepService {
  private endpoint: string;
  private httpOptions: any;

  constructor(private _globalService: GlobalService,
              private _http: HttpClient) {
                this.endpoint = `${this._globalService.GetEndpoint()}SalesRep/`;
                this.httpOptions = {
                  headers: this._globalService.headers
                };
              }
  
  public GetAll = (company: string): Observable<Array<SalesRep>> => {
    return this._http.get<Array<SalesRep>>(`${this.endpoint}All/${company}`, { headers: this.httpOptions });
  }

  public GetTeam = (username: string): Observable<string> => {
    return this._http.get<string>(`${this.endpoint}Team/${username}`, { headers: this.httpOptions });
  }
}
