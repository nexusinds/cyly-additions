import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Additions } from '../../models/additions';
import { GlobalService } from '../global/global.service';

@Injectable({
  providedIn: 'root'
})
export class AdditionService {
  private endpoint: string;
  private httpOptions: any;

  constructor(private _globalService: GlobalService,
              private _http: HttpClient) { 
                this.endpoint = `${this._globalService.GetEndpoint()}Addition/`;
                this.httpOptions = {
                  headers: this._globalService.headers
                };
  }

  public GetAll = (): Observable<Array<Additions>> => {
    return this._http.get<Array<Additions>>(`${this.endpoint}All`, { headers: this.httpOptions });
  }

  public Get = (id: number): Observable<Additions> => {
    return this._http.get<Additions>(`${this.endpoint}${id}`, { headers: this.httpOptions });
  }

  public GetHistory = (): Observable<Array<Additions>> => {
    return this._http.get<Array<Additions>>(`${this.endpoint}History`, { headers: this.httpOptions });
  }

  public Create = (model: Additions): Observable<Additions> => {
    return this._http.post<Additions>(`${this.endpoint}Create`, model, { headers: this.httpOptions })
                      .pipe(map((res) => {
                        return res;
                      }));
  }

  public Amend = (model: Additions): Observable<Additions> => {
    return this._http.post<Additions>(`${this.endpoint}Amend`, model, { headers: this.httpOptions })
                      .pipe(map((res) => {
                        return res;
                      }));
  }

  public Delete = (id: number): Observable<Additions> => {
    return this._http.post<Additions>(`${this.endpoint}Delete/${id}`, null, { headers: this.httpOptions })
                      .pipe(map((res) => {
                        return res;
                      }));
  }
}
