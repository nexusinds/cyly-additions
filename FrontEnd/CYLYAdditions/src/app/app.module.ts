import { BrowserModule }                    from '@angular/platform-browser';
import { BrowserAnimationsModule }          from '@angular/platform-browser/animations';
import { NgModule }                         from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule }                 from '@angular/common/http';

import { MatButtonModule }                  from '@angular/material/button';
import { MatProgressSpinnerModule }         from '@angular/material/progress-spinner';
import { MatTableModule }                   from '@angular/material/table';
import { MatFormFieldModule }               from '@angular/material/form-field';
import { MatInputModule }                   from '@angular/material/input';
import { MatPaginatorModule }               from '@angular/material/paginator';
import { MatSortModule }                    from '@angular/material/sort';
import { MatDialogModule }                  from '@angular/material/dialog';
import { MatSelectModule }                  from '@angular/material/select';
import { MatGridListModule }                from '@angular/material/grid-list';
import { MatTabsModule }                    from '@angular/material/tabs';
import { MatSnackBarModule }                from '@angular/material/snack-bar';
import { MatTooltipModule }                 from '@angular/material/tooltip';
import { MatMenuModule }                    from '@angular/material/menu';

import { AppComponent }                     from './components/app/app.component';
import { LoginComponent }                   from './components/login/login.component';
import { HomeComponent }                    from './components/home/home.component';
import { CreateDialogComponent }            from './components/create-dialog/create-dialog.component';
import { SuccessSnackBarComponent }         from './components/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent }           from './components/error-snack-bar/error-snack-bar.component';

import { AppRoutingModule }                 from './app.routing';
import { OauthhandshakeModule }             from './modules/oauthhandshake/oauthhandshake.module';

import { AuthenticationGuard }              from './guards/authentication/authentication.guard';

import { ConfigService }                    from './services/adal/config/config.service';
import { AdalService }                      from './services/adal/adal/adal.service';
import { HistoryComponent }                 from './components/history/history.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CreateDialogComponent,
    SuccessSnackBarComponent,
    ErrorSnackBarComponent,
    HistoryComponent
  ],
  imports: [
    // ANGULAR
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    // AUTH
    OauthhandshakeModule,
    // ANGULAR MATERIAL
    MatButtonModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatSelectModule,
    MatGridListModule,
    MatTabsModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatMenuModule
  ],
  providers: [
    AuthenticationGuard,
    ConfigService, 
    AdalService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    CreateDialogComponent,
    SuccessSnackBarComponent,
    ErrorSnackBarComponent
  ]
})
export class AppModule { }
