import { Component, OnInit, ViewChild } from '@angular/core';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Additions } from '../../models/additions';

import { AdditionService } from '../../services/additions/addition.service';
import { AdalService } from '../../services/adal/adal/adal.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  columns = [
    'Yr',
    'Mth',
    'SalesRepName',
    'SaleValue_g',
    'SaleCost_g',
    'SaleMargContr',
    'KFValue_g',
    'KFCost_g',
    'KFMargContr',
    'ClearanceValue_g',
    'ClearanceCost_g',
    'ClearanceMargContr',
    'OppositeSalesRepName',
    'Team',
    'Actions'
  ];

  _isLoading = true;
  _additions: MatTableDataSource<Additions>;
  @ViewChild(MatPaginator) _paginator: MatPaginator;
  @ViewChild(MatSort) _sort: MatSort;

  constructor(private _additionService: AdditionService,
              private _adalService: AdalService) { }

  ngOnInit() {
    if(this._adalService.accessToken == null) {
      this._adalService.login();
    }
    this.GetHistory();
  }

  GetHistory() {
    this._additionService.GetHistory()
                          .subscribe(adds => {
                            this._additions = new MatTableDataSource<Additions>(adds);
                            this._additions.sort = this._sort;
                            this._additions.paginator = this._paginator;
                            this._isLoading = false;
                          });
  }

  ApplyFilter(value: string) {
    value = value.trim();
    value = value.toLowerCase();
    this._additions.filter = value;
  }
}
