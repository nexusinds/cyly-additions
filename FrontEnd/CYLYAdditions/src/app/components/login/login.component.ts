import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AdalService } from '../../services/adal/adal/adal.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private adalService: AdalService) { }

  ngOnInit() {
    if (this.adalService.isAuthenticated) {
      this.router.navigate(['/home']);
    }
  }

  login() {
      this.adalService.login();
  }

  public get isLoggedIn() {
      return this.adalService.isAuthenticated;
  }
}
