import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { SalesRep } from '../../models/sales-rep';
import { BuyingGroups } from '../../models/buying-groups';

import { SalesRepService } from '../../services/sales-rep/sales-rep.service';
import { BuyingGroupsService } from '../../services/buying-groups/buying-groups.service';

@Component({
  selector: 'app-create-dialog',
  templateUrl: './create-dialog.component.html',
  styleUrls: ['./create-dialog.component.css']
})
export class CreateDialogComponent implements OnInit {
  CLEARANCE_COST_PCT = 65;
  PROJECT_COST_PCT = 63;
  _selectedCompany = "";
  _companies = new Array<any>();
  _years = new Array<number>();
  _months = new Array<any>();
  _salesReps: Array<SalesRep>;
  _oppSalesReps: Array<SalesRep>;
  _buyingGroups: Array<BuyingGroups>;
  _assumedRebate = 0.0;
  _assumedSettlement = 0.0;

  constructor(public dialogRef: MatDialogRef<CreateDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private _repService: SalesRepService, private _buyingGroupService: BuyingGroupsService) { }

  ngOnInit() {
    // console.log(this.data);
    this.GetSalesReps(null);
    this.GetBuyingGroups();
    this.InitCompanySelect();
    this.InitYearSelect();
    this.InitMonthSelect();
    if (this.data.Id === undefined) {
      this.InitValues();
    }
    this.SetYearMonthDefault();
  }

  InitYearSelect() {
    var minYear = new Date();
    minYear.setFullYear(minYear.getFullYear() - 5);
    var maxYear = new Date();
    maxYear.setFullYear(maxYear.getFullYear() + 5);
    for(var i = minYear.getFullYear(); i <= maxYear.getFullYear(); i++) {
      this._years.push(i);
    }
  }

  InitCompanySelect() {
    this._companies.push({key: null , value: "All"});
    this._companies.push({key: "LucecoAE" , value: "LucecoAE"});
    this._companies.push({key: "LucecoDE" , value: "LucecoDE"});
    this._companies.push({key: "LucecoES" , value: "LucecoES"});
    this._companies.push({key: "LucecoFR" , value: "LucecoFR"});
    this._companies.push({key: "LucecoHK" , value: "LucecoHK"});
    this._companies.push({key: "LucecoMX" , value: "LucecoMX"});
    this._companies.push({key: "NexusUS" , value: "NexusUS"});
  }

  InitMonthSelect() {
    this._months.push({key: 1, value: "January"});
    this._months.push({key: 2, value: "February"});
    this._months.push({key: 3, value: "March"});
    this._months.push({key: 4, value: "April"});
    this._months.push({key: 5, value: "May"});
    this._months.push({key: 6, value: "June"});
    this._months.push({key: 7, value: "July"});
    this._months.push({key: 8, value: "August"});
    this._months.push({key: 9, value: "September"});
    this._months.push({key: 10, value: "October"});
    this._months.push({key: 11, value: "November"});
    this._months.push({key: 12, value: "December"});
  }

  InitValues() {
    this.data.SaleValue_g = 0;
    this.data.SaleCost_g = 0;
    this.data.KFValue_g = 0;
    this.data.KFCost_g = 0;
    this.data.ClearanceValue_g = 0;
    this.data.ClearanceCost_g = 0;
    this.data.SaleMargContr = 0;
    this.data.KFMargContr = 0;
    this.data.ClearanceMargContr = 0;
  }

  SetYearMonthDefault() {
    if (this.data.Yr === 0) {
      this.data.Yr = new Date().getUTCFullYear();
      this.data.Mth = (new Date().getMonth() + 1);
    }
  }

  GetSalesReps(company: string) {
    this._repService.GetAll(company)
                    .subscribe(reps => {
                      this._salesReps = reps;
                      this._oppSalesReps = new Array<SalesRep>();
                      let norep = new SalesRep();
                      norep.Name = '';
                      this._oppSalesReps.push(norep);
                      reps.forEach(rep => {
                        this._oppSalesReps.push(rep);
                      });
                    });
  }

  GetBuyingGroups() {
    this._buyingGroupService.GetAll()
                    .subscribe(grps => {
                      this._buyingGroups = grps;
                    });
  }

  Save() {
    this.dialogRef.close();
  }

  Valid() {
    return this.data.SaleValue_g >= 0 &&
            this.data.SaleCost_g >= 0 &&
            this.data.KFValue_g >= 0 &&
            this.data.KFCost_g >= 0 &&
            this.data.ClearanceValue_g >= 0 &&
            this.data.ClearanceCost_g >= 0 &&
            this.data.SaleMargContr >= 0 &&
            this.data.KFMargContr >= 0 &&
            this.data.ClearanceMargContr >= 0;
  }

  GetTeam(event) {
    var salesRep = event.value;
    this._repService.GetTeam(salesRep)
                    .subscribe(team => {
                      console.log(team);
                      this.data.Team = team;
                    });
  }
}
