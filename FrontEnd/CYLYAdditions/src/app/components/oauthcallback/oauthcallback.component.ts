import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdalService } from '../../services/adal/adal/adal.service';

@Component({
  selector: 'app-oauthcallback',
  templateUrl: './oauthcallback.component.html',
  styleUrls: ['./oauthcallback.component.css']
})
export class OauthcallbackComponent implements OnInit {

  constructor(private router: Router, private adalService: AdalService) { }

  ngOnInit() {
    if (!this.adalService.userInfo) {
        this.router.navigate(['login']);
    } else {
        this.router.navigate(['home']);
    }
  }
}
