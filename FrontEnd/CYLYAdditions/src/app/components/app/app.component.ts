import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { AdalService } from '../../services/adal/adal/adal.service';
import { filter, map, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public currentUser: string = '';
  title = 'Home';
  currentYear = new Date().getUTCFullYear();
  currentRoute: string;

  constructor(private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _title: Title,
              public _adalService: AdalService) { }

  ngOnInit() {
    this._router.events
                .pipe(filter(event => event instanceof NavigationEnd))
                .pipe(map(() => this._activatedRoute))
                .pipe(map(route => {
                  while (route.firstChild) {
                    route = route.firstChild;
                  }
                  return route;
                }))
                .pipe(filter(route => route.outlet === 'primary'))
                .pipe(mergeMap(route => route.data))
                .subscribe((event) => {
                  this._title.setTitle(event['title']);
                  this.title = event['title'];
                  this.currentRoute = this._router.url;
                });
                
    console.log(this.title);
    console.log(this._adalService.isAuthenticated);
  }
  
  Logout() {
    this._adalService.logout();
  }
}
