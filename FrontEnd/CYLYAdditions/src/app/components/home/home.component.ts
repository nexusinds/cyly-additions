import { Component, OnInit, ViewChild } from '@angular/core';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Additions } from '../../models/additions';

import { AdditionService } from '../../services/additions/addition.service';
import { AdalService } from '../../services/adal/adal/adal.service';

import { CreateDialogComponent } from '../../components/create-dialog/create-dialog.component';
import { SuccessSnackBarComponent } from '../../components/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from '../../components/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'] 
})
export class HomeComponent implements OnInit {
  columns = [
    'Yr',
    'Mth',
    'SalesRepName',
    'SaleValue_g',
    'SaleCost_g',
    'SaleMargContr',
    'KFValue_g',
    'KFCost_g',
    'KFMargContr',
    'ClearanceValue_g',
    'ClearanceCost_g',
    'ClearanceMargContr',
    'OppositeSalesRepName',
    'Team',
    'Actions'
  ];

  _isLoading = true;
  _additions: MatTableDataSource<Additions>;
  @ViewChild(MatPaginator) _paginator: MatPaginator;
  @ViewChild(MatSort) _sort: MatSort;

  constructor(private _adalService: AdalService,
              private _additionService: AdditionService,
              private _createDialog: MatDialog,
              private _snackBar: MatSnackBar) { }

  ngOnInit() {
    if(this._adalService.accessToken == null) {
      this._adalService.login();
    }
    this.GetAdditions();
  }

  GetAdditions() {
    this._additionService.GetAll()
                          .subscribe(adds => {
                            this._additions = new MatTableDataSource<Additions>(adds);
                            this._additions.sort = this._sort;
                            this._additions.paginator = this._paginator;
                            this._isLoading = false;
                          });
  }

  ApplyFilter(value: string) {
    value = value.trim();
    value = value.toLowerCase();
    this._additions.filter = value;
  }
 
  LoadCreationDialog() {
    let newaddition = new Additions();
    newaddition.Yr = 0;
    let dialogRef = this._createDialog.open(CreateDialogComponent, {
      width: '1400px',
      height: '570px',
      data: newaddition,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result !== '') {
        this._additionService.Create(result)
                              .subscribe(data => {
                                if(data.Id !== undefined || data.Id === 0) {
                                  this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                                    duration: 1000,
                                    data: `This addition was saved successfully`,
                                    verticalPosition: 'top'
                                  });
                                  var oldData = this._additions.data;
                                  oldData.push(data);
                                  this._additions.data = oldData;
                                  this._additions.sort = this._sort;
                                  this._additions.paginator = this._paginator;
                                }
                                else {
                                  this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                                    duration: 1000,
                                    data: `An Error Occured while Saving this addition. Please check and try again`,
                                    verticalPosition: 'top'
                                  });
                                }
                              });
      }
    })
  }

  EditAddition(addition) {
    let dialogRef = this._createDialog.open(CreateDialogComponent, {
      width: '1400px',
      height: '570px',
      data: addition
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result !== '') {
        this._additionService.Amend(result)
                              .subscribe(data => {
                                if(data !== null) {
                                  this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                                    duration: 1000,
                                    data: `This addition was successfully edited`,
                                    verticalPosition: 'top'
                                  });
                                } else {
                                  this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                                    duration: 1000,
                                    data: `An Error Occured while Editing this addition. Please check and try again`,
                                    verticalPosition: 'top'
                                  });
                                }
                              });
      }
    })
  }

  DeleteAddition(id) {
    console.log(id);
    this._additionService.Delete(id)
        .subscribe(data => {
          let oldData = this._additions.data;
          oldData = oldData.filter(d => d.Id !== id);
          this._additions = new MatTableDataSource<Additions>(oldData);
          this._additions.sort = this._sort;
          this._additions.paginator = this._paginator;
          this._isLoading = false;
        })
  }
}
