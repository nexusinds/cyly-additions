import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OauthcallbackComponent } from './../../components/oauthcallback/oauthcallback.component';
import { OAuthCallbackHandler } from '../../guards/oauthcallback/oauthcallbackhandler.guard';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ OauthcallbackComponent],
  providers: [OAuthCallbackHandler]
})
export class OauthhandshakeModule { }
