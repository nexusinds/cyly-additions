import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { LoginComponent } from './components/login/login.component';
import { HistoryComponent } from './components/history/history.component';
import { HomeComponent } from './components/home/home.component';
import { OauthcallbackComponent } from './components/oauthcallback/oauthcallback.component';
import { AuthenticationGuard } from './guards/authentication/authentication.guard';
import { OAuthCallbackHandler } from './guards/oauthcallback/oauthcallbackhandler.guard';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginComponent,
        data: {
            title: ' Login'
        }
    },
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthenticationGuard],
        data: {
            title: ' Home'
        }
    },
    {
        path: 'history',
        component: HistoryComponent,
        data: {
            title: 'History'
        }
    },
    {
      path: 'id_token',
      component: OauthcallbackComponent,
      canActivate: [OAuthCallbackHandler]
    },
    {
      path: 'access_token',
      component: OauthcallbackComponent,
      canActivate: [OAuthCallbackHandler]
    }
];
@NgModule({
    providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
    imports: [RouterModule.forRoot(routes, { useHash: false })],
    exports: [RouterModule],
})
export class AppRoutingModule { }